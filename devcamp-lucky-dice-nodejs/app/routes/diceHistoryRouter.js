// Import bộ thư viện express
const express = require('express'); 

//Import middleware
const luckydiceMiddleware = require('../middlewares/luckydiceMiddleware')

//Tạo 1 instance router
const router = express.Router();

//Import controller
const {
  createDiceHistory,
  getAllDiceHistory,
  getDiceHistoryById,
  updateDiceHistoryById,
  deleteDiceHistoryById
} = require('../controllers/diceHistoryController');

router.use(luckydiceMiddleware); // Call middlewares

router.post('/users/:userId/dice-histories/', createDiceHistory);

router.get('/dice-histories', getAllDiceHistory);

router.get('/dice-histories/:diceHistoryId', getDiceHistoryById);

router.put('/dice-histories/:diceHistoryId', updateDiceHistoryById);

router.delete('/dice-histories/:diceHistoryId', deleteDiceHistoryById);

module.exports = router;