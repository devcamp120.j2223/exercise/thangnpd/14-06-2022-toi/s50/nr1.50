// B1: Import mongooseJS
const mongoose = require("mongoose");

// B2: Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// B3: Khởi tạo 1 schema với các thuộc tính được yêu cầu
const userSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId
  },
  username: {
    type: String, 
    unique: true, 
    required: true
  },
	firstname: {
    type: String, 
    required: true
  },
	lastname: {
    type:String, 
    required:true
  },
	createdAt: {
    type: Date, 
    default: Date.now()
  },
  updatedAt: {
    type: Date, 
    default: Date.now()
  }
});

// B4: Export ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("user", userSchema);