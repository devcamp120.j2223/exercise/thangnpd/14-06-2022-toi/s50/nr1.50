// B1: Import mongooseJS
const mongoose = require("mongoose");

// B2: Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// B3: Khởi tạo 1 schema với các thuộc tính được yêu cầu
const dicehistorySchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId
  },
  user: {
    type: mongoose.Types.ObjectId,
    ref: "user",
    required: true
  },
  dice: {
    type: Number, 
    required: true
  },
  createdAt: {
    type: Date, 
    default: Date.now()
  },
  updatedAt: {
    type: Date, 
    default: Date.now()
  }
})

// B4: Export ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("dicehistory", dicehistorySchema);